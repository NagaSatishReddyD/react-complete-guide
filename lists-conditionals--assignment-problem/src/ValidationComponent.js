import React from 'react';

const validationComponent = (props)=>{
    let paragraph = <p>Text long enough</p>
    if(props.inputLength <= 5){
        paragraph = <p>Text too short</p>
    }

    return (
        <div>
            {paragraph}
        </div>
    );
};

export default validationComponent;
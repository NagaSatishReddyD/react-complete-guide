import React from 'react';

const charComponent = (props)=>{
    const charStyle={
        display:'inline-block',
        padding:'16px',
        textAlign:'center',
        margin:'16px',
        border:'1px solid black'
    }

    return (
        <div style={charStyle} onClick={props.clicked}>
            <p>{props.data}</p>
        </div>
    );
};

export default charComponent